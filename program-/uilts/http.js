import configs from "./config.js";
import { formatUrl } from './uilts'
import Vue from 'vue'
//封装  get post delete put
export const http = ({ timeout, baseUrl, errorHandle, requestHandle }) => {
    const request = async (url, method = "get", data = {}, header = {}) => {
        // 定义请求对象
        const requestObj = {
            originUrl: url,
            baseUrl,
            url: `${baseUrl}${url}`,
            method,
            data,
            header,
            timeout
        }
        const config = requestHandle && requestHandle(requestObj) || requestObj;
        if (typeof config === 'object' && config.url && config.method) {
            try {
                const res = await uni.request(config);
                if (Array.isArray(res)) {
                    return res[1].data ? res[1].data : res[1]
                }
                //返回错误状态
                return Promise.reject(res)
            } catch (error) {
                errorHandle && errorHandle(error);
                return Promise.reject(error);
            }
        }
    }

    return {
        post(url, data, header = {}) {
            return request(url, 'post', data, header)
        },
        get(url, query = {}, header = {}) {
            //格式化请求地址
            const formatendUrl = formatUrl(url, query)
            return request(formatendUrl, 'get', {}, header)
        },
        put(url, data, header = {}) {
            return request(url, 'put', data, header)
        },
        delete(url, params, header = {}) {
            return request(`${url}/${params.id}`, 'delete', {}, header)
        }
    }
}

const request = http({
    timeout: 10000,
    baseUrl: configs.baseUrl,
    requestHandle: config => {
        //设置请求拦截
        //设置authoriz
        console.log(uni.getStorageSync('token'), '11111111');
        const headers = {
            "Authorization": "Bearer" + uni.getStorageSync('token')
        };
        //判断请求地址是否白名单中  在的话  删除对象中的Authorization
        if (configs.whiteList.includes(config.originUrl)) {
            delete headers["Authorization"]
        }
        return {
            ...config,
            header:headers
        }
    },
    errorHandle: error => {
        console.log(error,"error");
    }
})


Vue.prototype.$request = request;
export default request;
