export const formatUrl = (url,query)=>{


    return Object.keys(query).length?(url+"?"+Object.keys(query).map(key=>`${key}=${query[key]}`).join("&")):url;

    
}
export default function ({url='',query={},redirect=false,tabbar=false}){
   url = formatUrl(url, query);
   //跳转的页面是tabbar页面使用switchTab跳转
   //如果redirect值为true使用redirectTo跳转
   //以上两个条件都不符合使用navigateTo跳转并解决十次跳转的问题
   const methodName = tabbar ? 'switchTab' : redirect ? 'redirectTo' : 'navigateTo';
   return new Promise((resolve, reject) => {
     uni[methodName] ({
        url,
        success(res){
            resolve(res);
        },
        fail(error){
            reject(error);
        }
     })
   })
}
