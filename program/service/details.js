import request from '../uilts/http'

// 详情数据
export const requestDetail = (data) => request.get("/prod/prodInfo", data)
// 收藏 
export const requestCol=(data)=>request.post('/p/user/collection/addOrCancel',data)
// 评价接口
export const requestComm=(data)=>request.get('/prodComm/prodCommData',data)
// 商品规格接口
// export const requestSku=(data)=>request.get('/sku/getSkuList',data)
// 判断是否收藏
export const requestIsColl=(data)=>request.get('/p/user/collection/isCollection',data)