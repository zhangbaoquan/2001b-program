import http from '../uilts/http'

//获取提交订单数据
export const postOrderList=(data)=>http.post('/p/order/confirm',data);
//订单接口
export const postSubmit=(data)=>http.post('/p/order/submit',data);
//支付接口
export const postPay=(data)=>http.get('/p/order/pay',data);